@title[Introduction]

# Quick intro to React, React-Native, and GraphQL

---

@title[Before React]

### Before React

To add interactivity to a website,

* First we create a html file containing the elements we want to show
* Then we insert and run javascript to directly find and update the element on the page

+++

For example, to update a label everytime user types something to a textinput:

Source: index.html

```html
<p>Your name: <span id="text"></span></p>

<input type="text" name="name" id="text-input" />

<script src="scripts/index.js"></script>
```

+++

And a javascript file

```javascript
document.getElementById("text-input").addEventListener("keyup", function(e) {
  document.getElementById("text").innerHTML = e.target.value;
});
```

+++

![video](https://dl.dropboxusercontent.com/s/gn7cf7elv9vazrd/js-before-react.mp4)

---

@title[With React]

### With React

With React, we create the elements from React. And instead of updating and querying the elements directly, we update the **state**.

Our html file will only contain

```html
<div id="root"></div>
```

+++

And javascript file

```javascript
class Example extends React.Component {
  state = {
    text: ""
  };
  render() {
    return (
      <div>
        <p>Your name: {this.state.text}</p>
        <input
          type="text"
          onChange={e => this.setState({ text: e.target.value })}
        />
      </div>
    );
  }
}

ReactDOM.render(<Example />, document.getElementById("root"));
```

+++

```js
class Example extends React.Component {
```

We define a React component using JS class called `Example`. (There are several ways to define a React component).

+++
```js
state = {
  text: ""
};
```

A stateful React component, as the name implies, has a `state` property. React component created using JS class, like our `Example` component, is a stateful React component.

+++
```js
render() {
  return (
    // component here
  );
}
```

To render something on the screen, we have to implement the `render` function of a React component, and return an element (can be HTML element or another React component).

+++

```js
<input
  type="text"
  onChange={e => this.setState({ text: e.target.value })}
/>
```
To update the state, we have to call `this.setState()`. Setting or updating the state variable directly can result in unexpected behaviour.

+++

```js
<p>Your name: {this.state.text}</p>
```

Every time the state is updated, React will call the render function. In this case, when the `text` property in the `state` changes, the component will re-render.

+++

```js
ReactDOM.render(<Example />, document.getElementById("root"));
```

Render the React component to a html element with id called `root`.

---

@title[Passing dataReact components]

### Passing Data

To pass data from one component to another, we use the `props` property of a component. For example, let's define another component called **Greeting**. But this time, we create a stateless component. 

+++

```js
const Greeting = props => (
  <p>
    Hello, <strong>{props.name}</strong>
  </p>
);

```

A stateless component is just a normal javascript function that accepts `props` as its argument. In this case, Greeting component accepts a prop called `name` which is a property of `props` object.

+++

Then we render the Greeting component inside our Example component.

```js
render() {
  return (
    <div>
      <p>Your name: {this.state.text}</p>
      <Greeting name={this.state.text} />
      <input
        type="text"
        onChange={e => this.setState({ text: e.target.value })}
      />
    </div>
  );
}
```

+++

```js
<Greeting name={this.state.text} />
```
Since Greeting component accepts a prop called `name`, we render it in Example render function with the `name` prop. Then we assign the value of the prop from `this.state.text`. Thus, every time `this.state.text` value changes, the Greeting component will re-render.

+++

![video](https://dl.dropboxusercontent.com/s/vj4jai7hseumgep/js-with-react.mp4)

---

@title[Uni-direction data flow]

### Uni-directional Data Flow

In React, data flows only in one direction, from parent to children component. This is to enforce a single source of truth. In our example, the `Example` component is the parent of `Greeting` component. Thus, the Example component can pass data to Greeting component.

+++

To pass data from children component to the parent, the children needs to accept a prop with a `function` value. Let's see it in action.

+++

```javascript
const Greeting = ({name}) => <p>Hello, <strong>{name}</strong></p>
const MyInput = ({onChange}) => <input type="text" onChange={onChange} />

class Example extends React.Component {
  state = {
    text: ""
  }

  render() {
    return (
      <div>
        <p>Your name: {this.state.text }</p>
        <Greeting name={this.state.text} />
        <MyInput onChange={e => this.setState({text: e.target.value})} />
      </div>
    )
  }
}
```
@[2](Instead of rendering the input element directly in Example, we define a MyInput component that renders the input element.)
@[14](Then we pass a function to update the state of Example when user types something. We can see that here the data flows from children (MyInput) to parent (Example).)

---

@title[React Native]

### React-Native

React-Native (RN) is basically react but instead of rendering div, p, a, or any HTML elements, it renders native (iOS or Android) UI components, like UIView in iOS.

+++

```js
import React, { Component } from "react";
import { View, Image } from "react-native";

export default class Logo extends Component {
  render() {
    return (
      <View
        style={{
          ...this.props.style,
          backgroundColor: "white",
          opacity: 0.7,
          padding: 10,
          width: 184,
          height: 184,
          borderRadius: 92,
          justifyContent: "center"
        }}
      >
        <Image
          style={{ alignSelf: "center" }}
          source={require("../images/logo.png")}
        />
      </View>
    );
  }
}
```
In this example, we define a component called `Logo` and renders an image which is wrapped in a view with the defined style.

+++

The result

![image](https://dl.dropboxusercontent.com/s/7kn07zac98c4m3m/react-native-fab.png)

---

@title[GraphQL]

### GraphQL

GraphQL is a way to communicate between a client and a server. Traditionally, a client app will send a request to the server with some parameters, then the server will return a response. **BUT**, usually the server returns data more than what the client needs.

+++

For example, a client app sends a request to fetch a user data, the server will return everything about the user

```
GET /api/users/<some_user_id>

Response:
{
  _id: <some_user_id>,
  name: 'name',
  age: 21,
  address: 'someaddress'
}
```

But maybe the client app will not show the address of the user. So the address returned by the server is useless.

+++

With GraphQL, the client **tells** the server what it wants, then the server returns exactly what the client wants. 

+++

It does that by sending a query which will be processed by the server.

```graphql
query {
  User (id: "someid") {
    id
    email
    nickname
  }
}
```

+++

Response
```json
{
  "data": {
    "User": {
      "id": "someid",
      "email": "somemeail",
      "nickname": null
    }
  }
}
```